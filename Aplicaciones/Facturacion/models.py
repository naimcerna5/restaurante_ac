from django.db import models

class Provincia(models.Model):
    id_ac = models.AutoField(primary_key=True)
    nombre_ac = models.CharField(max_length=50)
    descripcion_ac = models.CharField(max_length=150)
    capital_ac = models.CharField(max_length=50)
    region_ac = models.CharField(max_length=50)

    def __str__(self):
        fila = "{0}:{1} {2} {3} "
        return fila.format(self.id_ac, self.nombre_ac, self.descripcion_ac, self.capital_ac, self.region_ac)
class Tipo(models.Model):
    # Definición de los campos del modelo Tipo
    id_ac = models.AutoField(primary_key=True)
    nombre_ac = models.CharField(max_length=50)
    descripcion_ac = models.CharField(max_length=150)
    fecha_creacion_ac = models.DateField()
    fotografia_ac = models.FileField(upload_to='tipo', null=True, blank=True)

    def __str__(self):
        fila = "{0}:{1} {2} {3}"
        return fila.format(self.id_ac, self.nombre_ac, self.descripcion_ac, self.fecha_creacion_ac)

class Cliente(models.Model):
    id_ac = models.AutoField(primary_key=True)
    nombre_ac = models.CharField(max_length=50)
    correo_ac = models.EmailField(unique=True)
    direccion_ac = models.TextField(blank=True, null=True)
    telefono_ac = models.CharField(max_length=20, blank=True, null=True)
    provincia_ac = models.ForeignKey(Provincia, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}:{1} {2} {3}"
        return fila.format(self.id_ac, self.nombre_ac, self.direccion_ac, self.telefono_ac)

class Pedido(models.Model):
    id_ac = models.AutoField(primary_key=True)
    nombre_ac = models.CharField(max_length=50)
    total_ac = models.CharField(max_length=50)
    fecha_pedido_ac = models.DateField()
    observaciones_ac = models.TextField()
    cliente_ac = models.ForeignKey(Cliente, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}:{1} {2} {3}"
        return fila.format(self.id_ac, self.nombre_ac, self.total_ac, self.fecha_pedido_ac, self.observaciones_ac)

class Platillo(models.Model):
    id_ac = models.AutoField(primary_key=True)
    nombre_ac = models.CharField(max_length=150)
    descripcion_ac = models.TextField()
    calorias_ac = models.PositiveIntegerField()
    fotografia_ac = models.FileField(upload_to='platillo', null=True, blank=True)
    tipo_ac = models.ForeignKey(Tipo, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}:{1} {2} {3} {4}"
        return fila.format(self.id_ac, self.nombre_ac, self.descripcion_ac, self.calorias_ac, self.tipo_ac)


class Detalle(models.Model):
    id_ac = models.AutoField(primary_key=True)
    cantidad_ac = models.PositiveIntegerField()
    precio_unitario_ac = models.CharField(max_length=50)
    subtotal_ac = models.CharField(max_length=50)
    comentarios_ac = models.TextField()
    pedido_ac = models.ForeignKey(Pedido, null=True, blank=True, on_delete=models.PROTECT)
    platillo_ac = models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}:{1} {2} {3} {4}"
        return fila.format(self.id_ac, self.cantidad_ac, self.precio_unitario_ac, self.subtotal_ac, self.comentarios_ac)


class Ingrediente(models.Model):
    id_ac = models.AutoField(primary_key=True)
    nombre_ac = models.TextField()
    descripcion_ac = models.CharField(max_length=150)
    fecha_expedicion_ac = models.DateField()
    fotografia_ac = models.FileField(upload_to='ingrediente', null=True, blank=True)

    def __str__(self):
        fila = "{0}:{1} {2} {3}"
        return fila.format(self.id_ac, self.nombre_ac, self.descripcion_ac, self.fecha_expedicion_ac)

class Receta(models.Model):
    id_ac = models.AutoField(primary_key=True)
    pasos_preparacion_ac = models.TextField()
    tiempo_preparacion_ac = models.PositiveIntegerField()
    porciones_ac = models.CharField(max_length=50)
    fecha_creacion_ac = models.DateField()
    platillo_ac = models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.PROTECT)
    ingrediente_ac = models.ForeignKey(Ingrediente, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}:{1} {2} {3} {4}"
        return fila.format(self.id_ac, self.pasos_preparacion_ac, self.tiempo_preparacion_ac, self.porciones_ac, self.fecha_creacion_ac)
