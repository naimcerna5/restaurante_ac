from django.urls import path
from . import views

urlpatterns=[
    path('',views.listadoProvincias),
    path('guardarProvincia/',views.guardarProvincia),
    path('eliminarProvincia/<id_ac>' ,views.eliminarProvincia),
    path('editarProvincia/<id_ac>' ,views.editarProvincia),
    path('procesarActualizacionProvincia/', views.procesarActualizacionProvincia),

    path('listadoClientes/',views.listadoClientes),
    path('eliminarCliente/<id_ac>' ,views.eliminarCliente),
    path('guardarCliente/',views.guardarCliente),
    path('editarCliente/<id_ac>' ,views.editarCliente),
    path('procesarActualizacionCliente/', views.procesarActualizacionCliente),

    path('listadoTipos/',views.listadoTipos),
    path('eliminarTipo/<id_ac>' ,views.eliminarTipo),
    path('guardarTipo/',views.guardarTipo),
    path('editarTipo/<id_ac>' ,views.editarTipo),
    path('procesarActualizacionTipo/', views.procesarActualizacionTipo),

    path('listadoPedidos/',views.listadoPedidos),
    path('eliminarPedido/<id_ac>' ,views.eliminarPedido),
    path('guardarPedido/',views.guardarPedido),
    path('editarPedido/<id_ac>' ,views.editarPedido),
    path('procesarActualizacionPedido/', views.procesarActualizacionPedido),

    path('listadoPlatillos/',views.listadoPlatillos),
    path('eliminarPlatillo/<id_ac>' ,views.eliminarPlatillo),
    path('guardarPlatillo/',views.guardarPlatillo),
    path('editarPlatillo/<id_ac>' ,views.editarPlatillo),
    path('procesarActualizacionPlatillo/', views.procesarActualizacionPlatillo),

    path('listadoDetalles/',views.listadoDetalles),
    path('eliminarDetalle/<id_ac>' ,views.eliminarDetalle),
    path('guardarDetalle/',views.guardarDetalle),
    path('editarDetalle/<id_ac>' ,views.editarDetalle),
    path('procesarActualizacionDetalle/', views.procesarActualizacionDetalle),

    path('listadoIngredientes/',views.listadoIngredientes),
    path('eliminarIngrediente/<id_ac>' ,views.eliminarIngrediente),
    path('guardarIngrediente/',views.guardarIngrediente),
    path('editarIngrediente/<id_ac>' ,views.editarIngrediente),
    path('procesarActualizacionIngrediente/', views.procesarActualizacionIngrediente),

    path('listadoRecetas/',views.listadoRecetas),
    path('eliminarReceta/<id_ac>' ,views.eliminarReceta),
    path('guardarReceta/',views.guardarReceta),
    path('editarReceta/<id_ac>' ,views.editarReceta),
    path('procesarActualizacionReceta/', views.procesarActualizacionReceta),

]
