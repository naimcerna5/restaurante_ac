from django.shortcuts import render, redirect
from .models import Provincia
from .models import Ingrediente
from .models import Cliente
from .models import Pedido
from .models import Platillo
from .models import Receta
from .models import Detalle
from .models import Tipo
from django.contrib import messages
from django.db import transaction
from django.utils.datastructures import MultiValueDictKeyError


def listadoProvincias(request):
    provinciasBdd = Provincia.objects.all()
    return render(request, 'listadoProvincias.html', {'provincias': provinciasBdd})

def guardarProvincia(request):

        # Asegúrate de utilizar las claves correctas del formulario
        nombre_ac = request.POST["nombre_ac"]
        descripcion_ac = request.POST["descripcion_ac"]
        capital_ac = request.POST["capital_ac"]
        region_ac = request.POST["region_ac"]

        # Crea una nueva instancia de Provincia
        provincia = Provincia.objects.create(
            nombre_ac=nombre_ac,
            descripcion_ac=descripcion_ac,
            capital_ac=capital_ac,
            region_ac=region_ac
        )

        # Mensaje de éxito y redirección

        messages.success(request, 'PROVINCIA GUARDADO EXITOSAMENTE')
        return redirect('/')

def eliminarProvincia(request, id_ac):
    provinciaEliminar = Provincia.objects.get(id_ac=id_ac)
    provinciaEliminar.delete()
    messages.success(request, 'PROVINCIA ELIMINADA EXITOSAMENTE')
    return redirect('/')

def editarProvincia(request, id_ac):
    provinciaEditar = Provincia.objects.get(id_ac=id_ac)
    provinciasBdd=Provincia.objects.all()
    return render(request, 'editarProvincia.html', {'provincia': provinciaEditar, 'provincias':provinciasBdd})

def procesarActualizacionProvincia(request):
    id_ac = request.POST["id_ac"]
    nombre_ac = request.POST["nombre_ac"]
    descripcion_ac = request.POST["descripcion_ac"]
    capital_ac = request.POST["capital_ac"]
    region_ac = request.POST["region_ac"]

    # Actualizando datos mediante el ORM de Django
    provinciaEditar = Provincia.objects.get(id_ac=id_ac)
    provinciaEditar.nombre_ac = nombre_ac
    provinciaEditar.descripcion_ac = descripcion_ac
    provinciaEditar.capital_ac = capital_ac
    provinciaEditar.region_ac = region_ac
    provinciaEditar.save()

    messages.success(request, 'PROVINCIA ACTUALIZADA Exitosamente')
    return redirect('/')

def listadoClientes(request):
    clientesBdd = Cliente.objects.all()
    provinciasBdd = Provincia.objects.all()
    return render(request, 'listadoClientes.html', {'clientes': clientesBdd, 'provincias': provinciasBdd})

def guardarCliente(request):
        id_provincia=request.POST["id_provincia"]
        provinciaSeleccionado=Provincia.objects.get(id_ac=id_provincia)

        nombre_ac = request.POST["nombre_ac"]
        correo_ac = request.POST["correo_ac"]
        direccion_ac =request.POST["direccion_ac"]
        telefono_ac=request.POST["telefono_ac"]
        # Insertando datos mediante el ORM de Django
        cliente = Cliente.objects.create(
        nombre_ac=nombre_ac,
        correo_ac=correo_ac,
        direccion_ac=direccion_ac,
        telefono_ac=telefono_ac,
        provincia_ac=provinciaSeleccionado)

        messages.success(request, 'CLIENTE GUARDADO EXITOSAMENTE')
        return redirect('/listadoClientes/')


def eliminarCliente(request, id_ac):
    clienteEliminar = Cliente.objects.get(id_ac=id_ac)
    clienteEliminar.delete()
    messages.success(request, 'CLIENTE ELIMINADO EXITOSAMENTE')
    return redirect('/listadoClientes/')

def editarCliente(request, id_ac):
    clienteEditar = Cliente.objects.get(id_ac=id_ac)
    clientesBdd = Cliente.objects.all()
    provinciasBdd = Provincia.objects.all()
    return render(request, 'editarCliente.html', {'cliente': clienteEditar, 'clientes': clientesBdd, 'provincias': provinciasBdd})

def procesarActualizacionCliente(request):
        id_ac = request.POST.get("id_ac")
        id_provincia = request.POST["id_provincia"]
        provinciaSeleccionado = Provincia.objects.get(id_ac=id_provincia)
        nombre_ac = request.POST["nombre_ac"]
        correo_ac = request.POST["correo_ac"]
        direccion_ac = request.POST["direccion_ac"]
        telefono_ac = request.POST["telefono_ac"]
        # Obtener el objeto EstadoCivil a actualizar
        clienteEditar = Cliente.objects.get(id_ac=id_ac)
        # Actualizar los campos del objeto
        clienteEditar.provincia_ac = provinciaSeleccionado  # Corregido el nombre del campo

        clienteEditar.nombre_ac = nombre_ac
        clienteEditar.correo_ac = correo_ac
        clienteEditar.direccion_ac = direccion_ac
        clienteEditar.telefono_ac = telefono_ac
        # Guardar los cambios en la base de datos
        clienteEditar.save()

        messages.success(request, 'CLIENTE ACTUALIZADO EXITOSAMENTE')
        return redirect('/listadoClientes/')


def listadoTipos(request):
    tiposBdd = Tipo.objects.all()

    return render(request, 'listadoTipos.html', {'tipos': tiposBdd})

def guardarTipo(request):

        nombre_ac = request.POST["nombre_ac"]
        descripcion_ac = request.POST["descripcion_ac"]
        fecha_creacion_ac = request.POST["fecha_creacion_ac"]
        fotografia_ac=request.FILES.get("fotografia_ac")
        # Insertando datos mediante el ORM de Django
        tipo = Tipo.objects.create(
            nombre_ac=nombre_ac,
            descripcion_ac=descripcion_ac,
            fecha_creacion_ac=fecha_creacion_ac,
            fotografia_ac=fotografia_ac
        )

        messages.success(request, 'TIPO DE ATENCION GUARDADA EXITOSAMENTE')
        return redirect('/listadoTipos/')

def eliminarTipo(request, id_ac):
    tipoEliminar = Tipo.objects.get(id_ac=id_ac)
    tipoEliminar.delete()
    messages.success(request, 'TIPO DE ATENCION ELIMINADA EXITOSAMENTE')
    return redirect('/listadoTipos/')

def editarTipo(request, id_ac):
    tipoEditar = Tipo.objects.get(id_ac=id_ac)
    tiposBdd=Tipo.objects.all()

    return render(request, 'editarTipo.html', {'tipo': tipoEditar, 'tipos':tiposBdd})

def procesarActualizacionTipo(request):
    id_ac = request.POST["id_ac"]
    nombre_ac = request.POST["nombre_ac"]
    descripcion_ac = request.POST["descripcion_ac"]
    fecha_creacion_ac = request.POST["fecha_creacion_ac"]
    fotografia_ac=request.POST["fotografia_ac"]

    tipoEditar = Tipo.objects.get(id_ac=id_ac)
    tipoEditar.nombre_ac = nombre_ac
    tipoEditar.descripcion_ac = descripcion_ac
    tipoEditar.fecha_creacion_ac = fecha_creacion_ac


    tipoEditar.save()
    messages.success(request, 'TIPO DE ATENCION ACTUALIZADA EXITOSAMENTE')
    return redirect('/listadoTipos/')



def listadoPedidos(request):
    pedidosBdd = Pedido.objects.all()
    clientesBdd = Cliente.objects.all()

    return render(request, 'listadoPedidos.html', {'pedidos': pedidosBdd, 'clientes': clientesBdd})

def guardarPedido(request):
    id_cliente = request.POST["id_cliente"]
    clienteSeleccionado = Cliente.objects.get(id_ac=id_cliente)

    nombre_ac = request.POST["nombre_ac"]
    total_ac = request.POST["total_ac"]
    fecha_pedido_ac = request.POST["fecha_pedido_ac"]
    observaciones_ac = request.POST["observaciones_ac"]

    # Insertando datos mediante el ORM de Django
    pedido = Pedido.objects.create(
        nombre_ac=nombre_ac,
        total_ac=total_ac,
        fecha_pedido_ac=fecha_pedido_ac,
        observaciones_ac=observaciones_ac,
        cliente_ac=clienteSeleccionado
    )

    messages.success(request, 'PEDIDO GUARDADO EXITOSAMENTE')
    return redirect('/listadoPedidos/')

def eliminarPedido(request, id_ac):
    pedidoEliminar = Pedido.objects.get(id_ac=id_ac)
    pedidoEliminar.delete()
    messages.success(request, 'PEDIDO ELIMINADO EXITOSAMENTE')
    return redirect('/listadoPedidos/')

def editarPedido(request, id_ac):
    pedidoEditar = Pedido.objects.get(id_ac=id_ac)
    pedidosBdd = Pedido.objects.all()
    clientesBdd = Cliente.objects.all()
    return render(request, 'editarPedido.html', {'pedido': pedidoEditar, 'pedidos': pedidosBdd, 'clientes': clientesBdd})

def procesarActualizacionPedido(request):
    id_ac = request.POST["id_ac"]
    nombre_ac = request.POST["nombre_ac"]
    total_ac = request.POST["total_ac"]
    fecha_pedido_ac = request.POST["fecha_pedido_ac"]
    observaciones_ac = request.POST["observaciones_ac"]
    id_cliente = request.POST["id_cliente"]  # Añadida la captura de id_cliente
    clienteSeleccionado = Cliente.objects.get(id_ac=id_cliente)

    # Insertando datos mediante el ORM de DJANGO
    pedidoEditar = Pedido.objects.get(id_ac=id_ac)
    pedidoEditar.cliente_ac = clienteSeleccionado
    pedidoEditar.nombre_ac = nombre_ac
    pedidoEditar.total_ac = total_ac
    pedidoEditar.fecha_pedido_ac = fecha_pedido_ac
    pedidoEditar.observaciones_ac = observaciones_ac
    pedidoEditar.save()
    messages.success(request, 'PEDIDO ACTUALIZADO EXITOSAMENTE')
    return redirect('/listadoPedidos/')
def listadoPlatillos(request):
    platillosBdd = Platillo.objects.all()
    tiposBdd = Tipo.objects.all()

    return render(request, 'listadoPlatillos.html', {'platillos': platillosBdd, 'tipos': tiposBdd})

def guardarPlatillo(request):
    id_tipo = request.POST["id_tipo"]
    tipo_seleccionado = Tipo.objects.get(id_ac=id_tipo)

    nombre_ac = request.POST["nombre_ac"]
    descripcion_ac = request.POST["descripcion_ac"]
    calorias_ac = request.POST["calorias_ac"]
    fotografia_ac = request.FILES.get("fotografia_ac")

    platillo = Platillo.objects.create(
        nombre_ac=nombre_ac,
        descripcion_ac=descripcion_ac,
        calorias_ac=calorias_ac,
        fotografia_ac=fotografia_ac,
        tipo_ac=tipo_seleccionado
    )

    messages.success(request, 'PLATILLO GUARDADO EXITOSAMENTE')
    return redirect('/listadoPlatillos/')

def eliminarPlatillo(request, id_ac):
    platilloEliminar = Platillo.objects.get(id_ac=id_ac)
    platilloEliminar.delete()
    messages.success(request, 'MASCOTA ELIMINADA EXITOSAMENTE')
    return redirect('/listadoPlatillos/')

def editarPlatillo(request, id_ac):
    platilloEditar = Platillo.objects.get(id_ac=id_ac)
    platillosBdd=Tipo.objects.all()
    tiposBdd=Tipo.objects.all()
    return render(request, 'editarPlatillo.html', {'platillo': platilloEditar,'platillos':platillosBdd, 'tipos':tiposBdd})

def procesarActualizacionPlatillo(request):
    id_ac = request.POST["id_ac"]
    id_tipo = request.POST["id_tipo"]

    tipoSeleccionado = Tipo.objects.get(id_ac=id_tipo)

    nombre_ac = request.POST["nombre_ac"]
    descripcion_ac = request.POST["descripcion_ac"]
    calorias_ac = request.POST["calorias_ac"]
    fotografia_ac=request.POST["fotografia_ac"]

    # Insertando datos mediante el ORM de DJANGO
    platilloEditar = Platillo.objects.get(id_ac=id_ac)
    platilloEditar.tipo_ac = tipoSeleccionado  # Corregido el nombre del campo
    platilloEditar.fotografia_ac=fotografia_ac

    platilloEditar.descripcion_ac = descripcion_ac
    platilloEditar.nombre_ac = nombre_ac
    platilloEditar.calorias_ac = calorias_ac
    platilloEditar.save()
    messages.success(request, 'PLATILLO ACTUALIZADA EXITOSAMENTE')
    return redirect('/listadoPlatillos/')


def listadoDetalles(request):
    detallesBdd = Detalle.objects.all()
    pedidosBdd = Pedido.objects.all()
    platillosBdd = Platillo.objects.all()
    return render(request, 'listadoDetalles.html', {'detalles': detallesBdd, 'platillos': platillosBdd, 'pedidos': pedidosBdd})

def guardarDetalle(request):
    id_platillo = request.POST["id_platillo"]
    id_pedido = request.POST["id_pedido"]
    platilloSeleccionado = Platillo.objects.get(id_ac=id_platillo)
    pedidoSeleccionado = Pedido.objects.get(id_ac=id_pedido)
    cantidad_ac = request.POST["cantidad_ac"]
    precio_unitario_ac = request.POST["precio_unitario_ac"]
    subtotal_ac = request.POST["subtotal_ac"]
    comentarios_ac = request.POST["comentarios_ac"]

    # Insertando datos mediante el ORM de Django
    detalle = Detalle.objects.create(
        cantidad_ac=cantidad_ac,
        precio_unitario_ac=precio_unitario_ac,
        subtotal_ac=subtotal_ac,
        comentarios_ac=comentarios_ac,
        platillo_ac=platilloSeleccionado,
        pedido_ac=pedidoSeleccionado
    )

    messages.success(request, 'DETALLE GUARDADO EXITOSAMENTE')
    return redirect('/listadoDetalles/')

def eliminarDetalle(request, id_ac):
    detalleEliminar = Detalle.objects.get(id_ac=id_ac)
    detalleEliminar.delete()
    messages.success(request, 'DETALLE ELIMINADO EXITOSAMENTE')
    return redirect('/listadoDetalles/')

def editarDetalle(request, id_ac):
    detalleEditar = Detalle.objects.get(id_ac=id_ac)
    detallesBdd = Detalle.objects.all()
    platillosBdd = Platillo.objects.all()
    pedidosBdd = Pedido.objects.all()
    return render(request, 'editarDetalle.html', {'detalle': detalleEditar, 'detalles': detallesBdd, 'platillos': platillosBdd, 'pedidos': pedidosBdd})

def procesarActualizacionDetalle(request):
    id_ac = request.POST["id_ac"]
    id_platillo = request.POST["id_platillo"]
    platilloSeleccionado = Platillo.objects.get(id_ac=id_platillo)
    id_pedido = request.POST["id_pedido"]
    pedidoSeleccionado = Pedido.objects.get(id_ac=id_pedido)

    cantidad_ac = request.POST["cantidad_ac"]
    precio_unitario_ac = request.POST["precio_unitario_ac"]
    subtotal_ac = request.POST["subtotal_ac"]
    comentarios_ac = request.POST["comentarios_ac"]

    # Insertando datos mediante el ORM de DJANGO
    detalleEditar = Detalle.objects.get(id_ac=id_ac)
    detalleEditar.platillo_ac = platilloSeleccionado
    detalleEditar.pedido_ac = pedidoSeleccionado
    detalleEditar.cantidad_ac = cantidad_ac
    detalleEditar.precio_unitario_ac = precio_unitario_ac
    detalleEditar.subtotal_ac = subtotal_ac
    detalleEditar.comentarios_ac = comentarios_ac

    detalleEditar.save()
    messages.success(request, 'DETALLE ACTUALIZADO EXITOSAMENTE')
    return redirect('/listadoDetalles/')

def listadoIngredientes(request):
    ingredientesBdd = Ingrediente.objects.all()
    return render(request, 'listadoIngredientes.html', {'ingredientes': ingredientesBdd})

def eliminarIngrediente(request, id_ac):
    ingredienteEliminar = Ingrediente.objects.get(id_ac=id_ac)
    ingredienteEliminar.delete()
    messages.success(request, 'INGREDIENTE ELIMINADO EXITOSAMENTE')
    return redirect('/listadoIngredientes/')

def guardarIngrediente(request):
    nombre_ac = request.POST["nombre_ac"]
    descripcion_ac = request.POST["descripcion_ac"]
    fecha_expedicion_ac = request.POST["fecha_expedicion_ac"]
    fotografia_ac = request.FILES.get("fotografia_ac")

    # Insertando datos mediante el ORM de django
    ingrediente = Ingrediente.objects.create(
        nombre_ac=nombre_ac,
        descripcion_ac=descripcion_ac,
        fecha_expedicion_ac=fecha_expedicion_ac,
        fotografia_ac=fotografia_ac
    )

    messages.success(request, 'INGREDIENTE GUARDADO EXITOSAMENTE')
    return redirect('/listadoIngredientes/')

def editarIngrediente(request, id_ac):
    ingredienteEditar = Ingrediente.objects.get(id_ac=id_ac)
    ingredientesBdd = Ingrediente.objects.all()
    return render(request, 'editarIngrediente.html', {'ingrediente': ingredienteEditar, 'ingredientes': ingredientesBdd})

def procesarActualizacionIngrediente(request):
    id_ac = request.POST["id_ac"]
    nombre_ac = request.POST["nombre_ac"]
    descripcion_ac = request.POST["descripcion_ac"]
    fecha_expedicion_ac = request.POST["fecha_expedicion_ac"]
    fotografia_ac = request.POST["fotografia_ac"]

    # Insertando datos mediante el ORM de DJANGO
    ingredienteEditar = Ingrediente.objects.get(id_ac=id_ac)
    ingredienteEditar.nombre_ac = nombre_ac
    ingredienteEditar.descripcion_ac = descripcion_ac
    ingredienteEditar.fecha_expedicion_ac = fecha_expedicion_ac
    ingredienteEditar.fotografia_ac = fotografia_ac
    ingredienteEditar.save()

    messages.success(request, 'INGREDIENTE ACTUALIZADO EXITOSAMENTE')
    return redirect('/listadoIngredientes/')
def listadoRecetas(request):
    recetasBdd = Receta.objects.all()
    ingredientesBdd = Ingrediente.objects.all()
    platillosBdd = Platillo.objects.all()

    return render(request, 'listadoRecetas.html', {'recetas': recetasBdd, 'ingredientes': ingredientesBdd, 'platillos': platillosBdd})

def eliminarReceta(request, id_ac):
    recetaEliminar = Receta.objects.get(id_ac=id_ac)
    recetaEliminar.delete()
    messages.success(request, 'RECETA ELIMINADA EXITOSAMENTE')
    return redirect('/listadoRecetas/')

def guardarReceta(request):
    id_platillo = request.POST["id_platillo"]
    id_ingrediente = request.POST["id_ingrediente"]
    # capturando el tipo seleccionado por el usuario
    platilloSeleccionado = Platillo.objects.get(id_ac=id_platillo)
    ingredienteSeleccionado = Ingrediente.objects.get(id_ac=id_ingrediente)

    pasos_preparacion_ac = request.POST["pasos_preparacion_ac"]
    tiempo_preparacion_ac = request.POST["tiempo_preparacion_ac"]
    porciones_ac = request.POST["porciones_ac"]
    fecha_creacion_ac = request.POST["fecha_creacion_ac"]

    # Insertando datos mediante el ORM de django
    receta = Receta.objects.create(
        pasos_preparacion_ac=pasos_preparacion_ac,
        tiempo_preparacion_ac=tiempo_preparacion_ac,
        porciones_ac=porciones_ac,
        fecha_creacion_ac=fecha_creacion_ac,
        platillo_ac=platilloSeleccionado,
        ingrediente_ac=ingredienteSeleccionado
    )
    messages.success(request, 'RECETA DEL PLATILLO GUARDADA EXITOSAMENTE')
    return redirect('/listadoRecetas/')

def editarReceta(request, id_ac):
    recetaEditar = Receta.objects.get(id_ac=id_ac)
    recetasBdd = Receta.objects.all()
    platillosBdd = Platillo.objects.all()
    ingredientesBdd = Ingrediente.objects.all()
    return render(request, 'editarReceta.html', {'receta': recetaEditar, 'recetas': recetasBdd, 'platillos': platillosBdd, 'ingredientes': ingredientesBdd})

def procesarActualizacionReceta(request):
    id_ac = request.POST["id_ac"]
    id_platillo = request.POST["id_platillo"]
    platilloSeleccionado = Platillo.objects.get(id_ac=id_platillo)
    id_ingrediente = request.POST["id_ingrediente"]
    ingredienteSeleccionado = Ingrediente.objects.get(id_ac=id_ingrediente)

    pasos_preparacion_ac = request.POST["pasos_preparacion_ac"]
    tiempo_preparacion_ac = request.POST["tiempo_preparacion_ac"]
    porciones_ac = request.POST["porciones_ac"]
    fecha_creacion_ac = request.POST["fecha_creacion_ac"]

    # Insertando datos mediante el ORM de DJANGO
    recetaEditar = Receta.objects.get(id_ac=id_ac)
    recetaEditar.platillo_ac = platilloSeleccionado
    recetaEditar.ingrediente_ac = ingredienteSeleccionado
    recetaEditar.pasos_preparacion_ac = pasos_preparacion_ac
    recetaEditar.tiempo_preparacion_ac = tiempo_preparacion_ac
    recetaEditar.porciones_ac = porciones_ac
    recetaEditar.fecha_creacion_ac = fecha_creacion_ac

    recetaEditar.save()

    messages.success(request, 'RECETA ACTUALIZADA EXITOSAMENTE')
    return redirect('/listadoRecetas/')
